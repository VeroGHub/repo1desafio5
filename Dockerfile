#Stage1: compilacion microservicio con maven para repo1
FROM adoptopenjdk/maven-openjdk8 AS builder
WORKDIR /home/root/build/
COPY . .
RUN mvn clean package

#Stage2: Copia del microservicio y ejecucion del jar para repo1
FROM adoptopenjdk/openjdk8
WORKDIR /home/root/desafio5/
COPY --from=builder /home/root/build/target/springboot-desafio5-repo1-0.0.1.jar /home/root/desafio5/
ENTRYPOINT ["java","-jar","springboot-desafio5-repo1-0.0.1.jar"]