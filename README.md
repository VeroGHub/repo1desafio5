# INSTRUCCIONES DESAFIO5

Este proyecto esta compuesto por 1 repositorio utilizando gitlab, para generar un microservicio diseñado en springboot java, además el despliegue se generó mediante contenedores docker el cual incluye un dockerfile para generar la imagen docker y un archivo docker-compose.yml para la ejecución del contenedor mediante compose, además se generó un despliegue mediante un pipeline gitlab-ci para desplegar el contenedor docker en una máquina cloud en Digital Ocean. Para la construcción de la imagen docker se usó un build multistage y en el despliegue en Digital Ocean se usó docker context.

Para validar el correcto funcionamiento del proyecto se puede realizar de las siguientes 3 formas:

1. Revisando servicios mediante vía web y/o cliente rest ej. Insomnia:
    
 - Ir a navegador web colocar la siguiente direccion http://104.248.124.71:8080, se desplegara el siguiente mensaje: "Estás viendo el desafio5 de Verito Diaz Repo1 GitLab en SpringBoot Java!!!!".

- Ir a cliente rest, posicionarse en opcion GET y colocar la siguiente url  http://104.248.124.71:8080, apretar boton SEND y en el response deberia aparecer el siguiente mensaje: "Estás viendo el desafio5 de Verito Diaz Repo1 GitLab en SpringBoot Java!!!!".

- Desde un terminal ejecutar un comando **curl 104.248.124.71:8080**, desplegara el siguiente mensaje "Estás viendo el desafio5 de Verito Diaz Repo1 GitLab en SpringBoot Java!!!!".

2. Descargar el repositorio y realizar pruebas con contenedores docker de forma local.

- Ir a Gitlab.com, ingresar con sus credenciales y descargar el siguiente repositorio https://gitlab.com/VeroGHub/repo1desafio5.git.
    
- Dentro del repositorio descargado generar la imagen docker de la siguiente forma:
    
    ***docker build -t (nombreimagen) .***
    
- Luego para levantar el  contenedor se puede realizar a través de la ejecución de este comando **docker-compose up -d** o a través de un **docker run -d --name (nombrecontenedor) -p (puertoaeleccion):8080 (nombreimagen)**.
    
- Luego revisar el resultado del microservicio mediante web: http://localhost:puertoaeleccion
    
3. Clonar repositorio y configurar el pipeline mediante gitlab-ci.
    
- Ir a Gitlab.com, ingresar con sus credenciales y clonar el siguiente repositorio https://gitlab.com/VeroGHub/repo1desafio5.git.
    
- Generar repositorio local y repositorio en gitlab.

- Instalar gitlab-runner en máquina local y configurarlo contra gitlab-ci (segun documentación de gitlab.com).
   
- Aprovisionar máquina en algún entorno cloud.

- Crear docker context en máquina local apuntando a máquina cloud (segun documentacion de docker.com).

- Modificar y adecuar archivo .gitlab-ci.yml (nombre imagen, credenciales para subir a registry y docker context).

- Hacer commit y push en el repositorio local.

- Validar ejecución del pipeline en gitlab-ci.

- Revisar los servicios desplegados en máquina cloud.
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    



